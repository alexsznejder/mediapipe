import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import CameraScreen from './src/screens/CameraScreen';
import { renderNavigatedApp } from './src/navigation/navigation';


const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <>
      {renderNavigatedApp()}
    </>
  );
};

export default App;
