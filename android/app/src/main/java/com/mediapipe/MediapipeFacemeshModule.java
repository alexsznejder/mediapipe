package com.mediapipe;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableNativeMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.module.annotations.ReactModule;
import com.google.mediapipe.solutions.facemesh.FaceMesh;
import com.google.mediapipe.solutions.facemesh.FaceMeshOptions;
import com.google.mediapipe.solutions.facemesh.FaceMeshResult;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmark;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


@ReactModule(name = MediapipeFacemeshModule.NAME)
public class MediapipeFacemeshModule extends ReactContextBaseJavaModule {
    public static final String NAME = "MediapipeFacemesh";
    private FaceMesh facemesh;
    private static final boolean RUN_ON_GPU = true;
//    private FaceMeshResultImageView imageView;

    public MediapipeFacemeshModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }


    // Example method
    // See https://reactnative.dev/docs/native-modules-android
    @ReactMethod
    public void multiply(int a, int b, Promise promise) {
        promise.resolve( a * b);
    }

    final AtomicReference<Object> reference = new AtomicReference<>(null);

    @ReactMethod
    public void runFaceMeshWithFiles(String base64Image, Promise promise) {
        reference.set(null);

        System.out.println("base64Image]");
        System.out.println(base64Image);


        facemesh =
                new FaceMesh(
                        this.getReactApplicationContext(),
                        FaceMeshOptions.builder()
//                                .setMode(FaceMeshOptions.STATIC_IMAGE_MODE)
                                .setRunOnGpu(RUN_ON_GPU)
                                .build());

        // Connects MediaPipe FaceMesh to the user-defined FaceMeshResultImageView.
        facemesh.setResultListener(
                faceMeshResult -> {
                    logNoseLandmark(faceMeshResult, /*showPixelValues=*/ true);

                    NormalizedLandmark[] kaczki = new NormalizedLandmark[faceMeshResult.multiFaceLandmarks().get(0).getLandmarkList().size()];
                    kaczki = faceMeshResult.multiFaceLandmarks().get(0).getLandmarkList().toArray(kaczki);
                    WritableArray promiseArray = Arguments.createArray();
                    for(int i = 0; i < kaczki.length; i++) {
                        promiseArray.pushString(String.format("%2f %2f", kaczki[i].getX(), kaczki[i].getY()));
                    }
                    reference.set(promiseArray);


//                    imageView.setFaceMeshResult(faceMeshResult);
//                    runOnUiThread(() -> imageView.update());
                });
        facemesh.setErrorListener((message, e) -> Log.e(NAME, "MediaPipe FaceMesh error:" + message));

        byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        facemesh.send(decodedByte);


        do{
            if(reference.get() != null) {
                System.out.println("promise.resolve(reference.get())");
                promise.resolve(reference.get());
            }
        } while(reference.get() == null);
    }

    private void logNoseLandmark(FaceMeshResult result, boolean showPixelValues) {
        if (result == null || result.multiFaceLandmarks().isEmpty()) {
            return;
        }
        NormalizedLandmark noseLandmark = result.multiFaceLandmarks().get(0).getLandmarkList().get(1);
        // For Bitmaps, show the pixel values. For texture inputs, show the normalized coordinates.
        if (showPixelValues) {
            int width = result.inputBitmap().getWidth();
            int height = result.inputBitmap().getHeight();
            Log.i(
                    NAME,
                    String.format(
                            "MediaPipe FaceMesh nose coordinates (pixel values): x=%f, y=%f",
                            noseLandmark.getX() * width, noseLandmark.getY() * height));
        } else {
            Log.i(
                    NAME,
                    String.format(
                            "MediaPipe FaceMesh nose normalized coordinates (value range: [0, 1]): x=%f, y=%f",
                            noseLandmark.getX(), noseLandmark.getY()));
        }
    }


    public static native int nativeMultiply(int a, int b);
}
