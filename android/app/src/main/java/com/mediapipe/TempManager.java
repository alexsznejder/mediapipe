package com.mediapipe;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.facebook.react.bridge.ReactContext;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.google.mediapipe.formats.proto.LandmarkProto;
import com.google.mediapipe.solutioncore.CameraInput;
import com.google.mediapipe.solutioncore.SolutionGlSurfaceView;
import com.google.mediapipe.solutions.facemesh.FaceMesh;
import com.google.mediapipe.solutions.facemesh.FaceMeshOptions;
import com.google.mediapipe.solutions.facemesh.FaceMeshResult;

public class TempManager extends SimpleViewManager<FrameLayout> {

  public Activity mActivity = null;

  public TempManager(ReactContext reactContext) {
    mActivity = reactContext.getCurrentActivity();
  }

  public static final String REACT_CLASS = "SimpleTextView";

  private enum InputSource {
    UNKNOWN,
    IMAGE,
    VIDEO,
    CAMERA,
  }

  private FaceMesh facemesh;
  private static final boolean RUN_ON_GPU = true;
  private InputSource inputSource = InputSource.UNKNOWN;
  private CameraInput cameraInput;
  private SolutionGlSurfaceView<FaceMeshResult> glSurfaceView;
  public FrameLayout frameLayout = null;

  @Override
  public String getName() {
    return REACT_CLASS;
  }

  @Override
  protected FrameLayout createViewInstance(ThemedReactContext reactContext) {
    frameLayout = new FrameLayout(reactContext);
    runFaceMeshWithStream(InputSource.CAMERA);
    return frameLayout;
  }

  private void runFaceMeshWithStream(InputSource inputSource) {
    this.inputSource = inputSource;
    // Initializes a new MediaPipe FaceMesh instance in the streaming mode.
    facemesh =
            new FaceMesh(
                    mActivity,
                    FaceMeshOptions.builder()
                            .setRunOnGpu(RUN_ON_GPU)
                            .build());
//        facemesh.setErrorListener((message, e) -> Log.e(TAG, "MediaPipe FaceMesh error:" + message));

    // Initializes a new CameraInput instance and connects it to MediaPipe FaceMesh.
    cameraInput = new CameraInput(mActivity);
    cameraInput.setNewFrameListener(textureFrame -> facemesh.send(textureFrame));

    // Initializes a new Gl surface view with a user-defined FaceMeshResultGlRenderer.
    glSurfaceView =
            new SolutionGlSurfaceView<>(mActivity, facemesh.getGlContext(), facemesh.getGlMajorVersion());
    glSurfaceView.setSolutionResultRenderer(new FaceMeshResultGlRenderer());
    glSurfaceView.setRenderInputImage(true);
    facemesh.setResultListener(
            faceMeshResult -> {
              logNoseLandmark(faceMeshResult, /*showPixelValues=*/ false);
              glSurfaceView.setRenderData(faceMeshResult);
              glSurfaceView.requestRender();
            });

    // The runnable to start camera after the gl surface view is attached.
    // For video input source, videoInput.start() will be called when the video uri is available.
    if (inputSource == InputSource.CAMERA) {
      glSurfaceView.post(this::startCamera);
    }

    // Updates the preview layout.
    frameLayout.removeAllViewsInLayout();
    frameLayout.addView(glSurfaceView);
    glSurfaceView.setVisibility(View.VISIBLE);
    frameLayout.requestLayout();
  }

  private void startCamera() {
    cameraInput.start(
            mActivity,
            facemesh.getGlContext(),
            CameraInput.CameraFacing.FRONT,
            glSurfaceView.getWidth(),
            glSurfaceView.getHeight());
  }

  private void stopCurrentPipeline() {
    if (cameraInput != null) {
      cameraInput.setNewFrameListener(null);
      cameraInput.close();
    }
    if (glSurfaceView != null) {
      glSurfaceView.setVisibility(View.GONE);
    }
    if (facemesh != null) {
      facemesh.close();
    }
  }

  private void logNoseLandmark(FaceMeshResult result, boolean showPixelValues) {
    if (result == null || result.multiFaceLandmarks().isEmpty()) {
      return;
    }
    LandmarkProto.NormalizedLandmark noseLandmark = result.multiFaceLandmarks().get(0).getLandmarkList().get(1);
    // For Bitmaps, show the pixel values. For texture inputs, show the normalized coordinates.
    if (showPixelValues) {
      int width = result.inputBitmap().getWidth();
      int height = result.inputBitmap().getHeight();
      Log.i(
              REACT_CLASS,
              String.format(
                      "MediaPipe FaceMesh nose coordinates (pixel values): x=%f, y=%f",
                      noseLandmark.getX() * width, noseLandmark.getY() * height));
    } else {
      Log.i(
              REACT_CLASS,
              String.format(
                      "MediaPipe FaceMesh nose normalized coordinates (value range: [0, 1]): x=%f, y=%f",
                      noseLandmark.getX(), noseLandmark.getY()));
    }
  }
}
