package com.mediapipe;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class CameraFacemeshModule extends ReactContextBaseJavaModule {

    ReactApplicationContext context = getReactApplicationContext();
    public CameraFacemeshModule(@NonNull ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @NonNull
    @Override
    public String getName() {
        return "CameraFacemesh";
    }

    @ReactMethod
    public void openCamera() {
        Intent intent = new Intent(context, CameraActivity.class);
        if(intent.resolveActivity(context.getPackageManager()) != null) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}
