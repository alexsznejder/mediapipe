import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import screens from './screens';
import MainScreen from '../screens/MainScreen';
// import {LocalizationContext} from '../../App';
import ImagePickerScreen from '../screens/ImagePickerScreen';
import CameraScreen from '../screens/CameraScreen';


export function renderNavigatedApp() {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={'MainScreen'}>
        <Stack.Screen
          name={screens.MainScreen}
          component={MainScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name={screens.ImagePickerScreen}
          component={ImagePickerScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name={screens.CameraScreen}
          component={CameraScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
