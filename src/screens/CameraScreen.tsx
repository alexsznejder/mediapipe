import React, {useState} from 'react';
import {
  Text,
  View,
  requireNativeComponent,
  ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';


const CameraScreen = () => {

  const viewProps = {
    name: 'SimpleTextView',
    propTypes: {
      text: PropTypes.string,
      ...ViewPropTypes,
    }
  }

  const CameraFacemeshComponent = requireNativeComponent('SimpleTextView', viewProps);

  return (
    <View style={{padding: 20}}>
      <View style={{width: 100, height: 100, position: 'absolute', backgroundColor: 'yellow', zIndex: 1, top: 200, left: 200}}>
        <Text style={{fontSize: 20, fontWeight: 'bold', color: 'pink'}}>
          {'Absolute'}
        </Text>
      </View>
      <CameraFacemeshComponent style={{ width: '100%', height: '100%', backgroundColor: 'pink'}}/>
    </View>
  );
};

export default CameraScreen;
