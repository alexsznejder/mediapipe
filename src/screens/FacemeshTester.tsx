import * as React from 'react';
import {launchImageLibrary} from 'react-native-image-picker';

import { StyleSheet, View, Text, Alert, Button, Image, StyleProp, ViewStyle, NativeModules } from 'react-native';

const FacemeshTester = React.memo((props: {
  image?: {uri: string, base64: string}
}) => {
  const { image } = props;
  const [result, setResult] = React.useState<string[]>();
  // const [result, setResult] = React.useState<number[][][][]>();
  const {MediapipeFacemesh} = NativeModules;

  return <View>
    <Button title='Run Facemesh' onPress={async () => {
      if (!image) return;
      console.log('before MediapipeFacemesh.runFaceMeshWithFiles')
      const res = await MediapipeFacemesh.runFaceMeshWithFiles(image.base64);
      console.log('res')
      console.log(res)
      setResult(formatResponse(res));
    }} />
    <RenderFaceBox
      points={result}
      imageUri={image.uri}
    />
    <Image source={{ uri: image.uri }} style={{ width: 320, height: 320 }} resizeMode='contain' />
  </View>
})

function formatResponse(response: String[]) {
  const mappedResponse = response.map(point => {
    const splitedPoint = String(point).split(' ');
    return {
      x: splitedPoint[0],
      y: splitedPoint[1],
    }
  });

  return mappedResponse;
}

function getShape(obj:any[]): number[]|undefined {
  const dims : number[] = [];
  if(obj.length) {
    dims.push(obj.length);
    if(obj.length > 0) {
      const nextShapes = getShape(obj[0]);
      if(nextShapes) {
        dims.push(...nextShapes);
      }
    }
    return dims;
  }
  else {
    return undefined;
  }
}
export default FacemeshTester;


export const RenderFaceBox = React.memo((props: {
  points?: {x: string, y: string}[],
  style?: StyleProp<ViewStyle>,
  imageUri?: string
}) => {
  const {
    style,
    points,
    imageUri
  } = props;
  const [imSize, setImSize] = React.useState({ width: 250, height: 250 });

  React.useEffect(() => {
    if (!imageUri) return;
    (async () => {
      const prom = new Promise<{ width: number, height: number }>((resolve, reject) => {
        Image.getSize(imageUri, (w, h) => {
          resolve({ width: w, height: h });
        }, reject);
      });
      const size = await prom;
      setImSize(size);
    })();
  }, [imageUri]);

  const landmarkCircles = React.useMemo(()=>{
    if(!points) return undefined;
    const datalen = points.length;
    const landmarkCircles: JSX.Element[] = [];
    for (let i = 0; i < datalen; i += 1) {
      const point = points[i];
      const x = Number(point.x.replace(',', '.'));
      const y = Number(point.y.replace(',', '.'));

      landmarkCircles.push(<View
        key={`landmark_${i}`}
        style={{
          width: 1.5, height: 1.5, borderRadius: 0.5,
          left: `${x * 100}%`,
          top: `${y * 100}%`,
          backgroundColor: 'blue',
          position: 'absolute'
        }}
      />);
    }

    return landmarkCircles;
  }, [points]);

  return <View
    style={[{ width: 250, height: 250 }, style]}
  >
    <Image source={{ uri: imageUri }} style={{ aspectRatio: imSize.width / imSize.height, flex: 1 }} resizeMode='contain' />
    <View style={StyleSheet.absoluteFill} pointerEvents='none'>
      <View style={{ aspectRatio: imSize.width / imSize.height, flex: 1 }}>
        {landmarkCircles}
      </View>
    </View>
  </View>;
})