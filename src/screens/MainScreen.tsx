import {
  NavigationProp,
  ParamListBase,
  useNavigation,
} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import screens from '../navigation/screens';


const MainScreen = () => {
  const navigation: NavigationProp<ParamListBase> = useNavigation();
  const [showAbsolute, setShowAbsolute] = useState(false);
  console.log('!!!!!!!!!!!!!!!!!!!!!!!!')
  return (
    <View style={{flex: 1}}>
      {showAbsolute &&
        <View style={{width: 200, height: 200, position: 'absolute'}}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'pink'}}>
            {'Absolute'}
          </Text>
        </View>
      }
      <TouchableOpacity onPress={() => navigation.navigate(screens.ImagePickerScreen)}
        style={{padding: 12, borderWidth: 1, borderRadius: 8, marginTop: 30, backgroundColor: 'white'}}>
        <Text style={{fontSize: 18}}>{'Image Picker'}</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate(screens.CameraScreen)}
        style={{padding: 12, borderWidth: 1, borderRadius: 8, marginTop: 30, backgroundColor: 'white'}}>
        <Text style={{fontSize: 18}}>{'Camera'}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default MainScreen;
