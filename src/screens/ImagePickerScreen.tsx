import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Alert,
  Image,
  NativeModules,
} from 'react-native';
// import MediapipeFacemesh from "react-native-mediapipe-facemesh";
import {launchImageLibrary} from 'react-native-image-picker';
import FacemeshTester from './FacemeshTester';


const ImagePickerScreen = () => {
  const [imgPath, setImgPath] = React.useState<any>({
    assets: [{
      uri: '',
      // 'https://cdn0.iconfinder.com/data/icons/user-pictures/100/female-256.png',
      base64: ''
    }]
  });
  console.log('NativeModules')
  console.log(NativeModules)
  const {MediapipeFacemesh} = NativeModules;
  const [facemeshResponse, setFacemeshResponse] = React.useState<any>(null)

  async function pickImage() {

    launchImageLibrary({
        maxHeight: 200,
        maxWidth: 200,
        selectionLimit: 0,
        mediaType: 'photo',
        includeBase64: true,
      }, response => {
        if (!response.didCancel) {
          console.log('response')
          console.log(response)
          setImgPath(response);
        }
      },);
  }

  return (
    <>
      <TouchableOpacity onPress={async () => {
        await MediapipeFacemesh.runFaceMeshWithFiles(imgPath.assets[0].base64).then((result) => {
          setFacemeshResponse(result);
          console.log('result');
          console.log(result);
          console.log(JSON.stringify(result))});
        // await MediapipeFacemesh.runFaceMeshWithFiles(7).then((result) => console.log(result));
        Alert.alert('OK');
      }}
        style={{padding: 12, borderWidth: 1, borderRadius: 8, backgroundColor: 'white'}}>
        <Text style={{fontSize: 18}}>{'Try alloc'}</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={async () => pickImage()}
        style={{padding: 12, borderWidth: 1, borderRadius: 8, marginTop: 30, backgroundColor: 'white'}}>
        <Text style={{fontSize: 18}}>{'Pick image'}</Text>
      </TouchableOpacity>

      <FacemeshTester image={imgPath.assets[0]} />
    </>
  );
};

export default ImagePickerScreen;
